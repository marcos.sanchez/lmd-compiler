#!/usr/bin/env sh

go get -v github.com/Jeffail/gabs \
  && go get -v github.com/kdar/factorlog \
  && go get -v github.com/prometheus/client_golang/prometheus \
  && go get -v github.com/BurntSushi/toml \
  && go get -v github.com/julienschmidt/httprouter;

go get -v github.com/sni/lmd/lmd

cd $GOPATH/src/github.com/sni/lmd/ \
    && git checkout $LMD_VERSION \
    && for PF in /patches/*.patch; do git apply $PF; done \
    && go build -o /binaries/lmd.bin ./lmd




